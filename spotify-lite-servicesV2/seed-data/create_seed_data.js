const faker = require("faker");
const jsonfile = require("jsonfile");
const _ = require("lodash");
const mp3Duration = require("mp3-duration");
const AWS = require("aws-sdk");
const listAllObjects = require("s3-list-all-objects");
const fetch = require("node-fetch");

const s3 = new AWS.S3();
const s3Bucket = "sp-lite-music";
class SeedDataGenerator {
  constructor() {
    this.generateGenreData();
  }

  getAlbumArtworks(genres, artistData) {
    console.info("genrating album artworks data...");

    const imageWidth = 480; //your desired image width in pixels
    const imageHeight = 480; //desired image height in pixels
    const collectionID = 1301616; //the collection ID from the original url
    const imgsPromises = [];
    const numItemsToGenerate = 200;

    for (let i = 0; i < numItemsToGenerate; i++) {
      let randomImageIndex = Math.floor(Math.random() * 500);
      imgsPromises.push(
        fetch(
          `https://source.unsplash.com/collection/${collectionID}/${imageWidth}x${imageHeight}/?sig=${randomImageIndex}`
        )
      );
    }

    Promise.all(imgsPromises).then(data => {
      const urls = data.map(d => d.url);
      this.generateAlbumData(genres, artistData, urls);
    });
  }
  getMp3Duration(path) {
    return new Promise((resolve, reject) => {
      var s3Params = {
        Bucket: s3Bucket,
        Key: path
      };
      s3.getObject(s3Params, function(err, res) {
        if (err) reject(err);
        mp3Duration(res.Body, function(err, duration) {
          if (err) reject(err);
          resolve({
            path,
            duration
          });
        });
      });
    });
  }

  getMp3s(albumsData) {
    console.info("genrating mp3  data...");

    let musicFilesProms = [];
    const $this = this;
    listAllObjects({ bucket: s3Bucket, prefix: "tracks/" }, function(
      err,
      data
    ) {
      if (err) return console.log(err);
      data.forEach(d => {
        const path = d.Key;
        musicFilesProms.push($this.getMp3Duration(path));
      });
      return Promise.all(musicFilesProms).then(function(mp3s) {
        mp3s = mp3s.filter(mp3 => mp3.duration > 0);

        $this.generateTracksForEachAlbum(albumsData, mp3s);
      });
    });
  }

  generateGenreData() {
    console.info("genrating genre data...");
    const genres = ["pop", "hip hop", "rock", "country", "R&B", "Reagea"];
    const gfile = "Genres.json";
    const gdata = genres.map(genre => {
      return {
        genre_id: faker.random.uuid(),
        genre_name: genre
      };
    });

    jsonfile.writeFileSync(gfile, gdata, function(err) {
      if (err) {
        console.error(err);
      } else {
        console.log("data created successfully");
      }
    });
    this.generateArtistData(gdata);
  }

  generateArtistData(genres) {
    console.info("genrating artists data...");

    const artistFile = "Artists.json";
    const artistCt = 50;
    const artistData = new Array(artistCt).fill(undefined).map(() => {
      return {
        artist_id: faker.random.uuid(),
        artist_name: faker.name.findName()
      };
    });

    jsonfile.writeFileSync(artistFile, artistData, function(err) {
      if (err) {
        console.error(err);
      } else {
        console.log("artists data created successfully");
      }
    });

    this.getAlbumArtworks(genres, artistData);
  }

  async generateAlbumData(genres, artists, images) {
    console.info("genrating album  data...");

    const albumsFile = "Albums.json";
    const albumCt = 500;
    const albumsData = new Array(albumCt).fill(undefined).map((x, i) => {
      const artist_id = _.sample(artists).artist_id;
      const genre_id = _.sample(genres).genre_id;
      const album_artwork = _.sample(images);
      const album_type = i % 25 === 0 ? "Ep" : "Full";
      return {
        album_id: faker.random.uuid(),
        album_name: faker.lorem.sentence(
          faker.random.number({ min: 1, max: 5 })
        ),
        genre_id,
        artist_id,
        album_artwork,
        album_type
      };
    });

    jsonfile.writeFileSync(albumsFile, albumsData, function(err) {
      if (err) {
        console.error(err);
      } else {
        console.log("artists data created successfully");
      }
    });
    this.getMp3s(albumsData);
  }

  generateTracksForEachAlbum(albums, mp3s) {
    console.info("genrating tracks  data...");
    const tracksFile = "Tracks.json";
    let tracks = [];
    albums.forEach(album => {
      const trackCt =
        album.album_type === "Ep"
          ? faker.random.number({ min: 3, max: 4 })
          : faker.random.number({ min: 5, max: 15 });
      const albumTracks = _.sampleSize(mp3s, trackCt).map((mp3, i) => {
        const is_single = i % 4 === 0;
        return {
          track_id: faker.random.uuid(),
          album_id: album.album_id,
          track_name: faker.company.catchPhrase(),
          artist_id: album.artist_id,
          genre_id: album.genre_id,
          album_order: i + 1,
          track_path: mp3.path,
          track_duration: mp3.duration,
          is_single
        };
      });
      tracks = [...tracks, ...albumTracks];
    });

    jsonfile.writeFileSync(tracksFile, tracks, function(err) {
      if (err) {
        console.error(err);
      } else {
        console.log("tracks data created successfully");
      }
    });
  }
}

new SeedDataGenerator();
