const gdata = require("./Genres.json");
const artistData = require("./Artists.json");
const albumData = require("./Albums.json");
const tracksData = require("./Tracks.json");

const knex = require("knex")({
  client: "sqlite3",
  connection: {
    filename: "../dev.db"
  }
});

const genres = gdata.map(a => a);
const artists = artistData.map(a => a);
const albums = albumData.map(a => a);
const tracks = tracksData.map(a => a);
console.log("numof tracks ", tracks.length);
const insertData = function() {
  return knex("Genres")
    .del()
    .then(() => knex("Artists").del())
    .then(() => knex("Albums").del())
    .then(() => knex("Tracks").del())
    .then(() => knex("Genres").insert(genres))
    .then(() => knex("Artists").insert(artists))
    .then(() => knex.batchInsert("Albums", albums, 100))
    .then(() => knex.batchInsert("Tracks", tracks, 100));

  //.then(() => knex("Albums").insert(albums))

  //.then(() => knex("Tracks").insert(tracks));
};

insertData()
  .then(function() {
    console.log("Data Insertion Complete");
  })
  .then(function() {
    process.exit(0);
  })
  .catch(function(error) {
    console.log(error);
    process.exit(0);
  });
