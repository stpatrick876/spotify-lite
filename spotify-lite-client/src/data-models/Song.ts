export class Song {
  track_id: string;
  album_id: string;
  track_name: string;
  track_plays: string;
  track_path: string;
  artist_name: string;
  featuredArtist?: string[];
  soundId?: number; // howler sound id
  track_duration?: string;
}
