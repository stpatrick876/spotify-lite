import { Injectable } from "@angular/core";
import * as firebase from "firebase/app";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireAuth } from "angularfire2/auth";
import { Router } from "@angular/router";
import { switchMap, map } from "rxjs/operators";
import { of } from "rxjs";
@Injectable({
  providedIn: "root"
})
export class AuthenticationService {
  constructor(
    private af: AngularFireDatabase,
    public _afAuth: AngularFireAuth,
    private _router: Router
  ) {}

  doRegister(value) {
    return new Promise<any>((resolve, reject) => {
      firebase
        .auth()
        .createUserWithEmailAndPassword(value.email, value.password1)
        .then(
          res => {
            this.af
              .object(`/users/${res.user.uid}`)
              .update({
                username: value.username,
                firstname: value.firstname,
                lastname: value.lastname
              })
              .then(res => resolve(res), err => reject(err));
            resolve(res);
          },
          err => reject(err)
        );
    });
  }

  doLogin(value) {
    return new Promise<any>((resolve, reject) => {
      firebase
        .auth()
        .signInAndRetrieveDataWithEmailAndPassword(value.email, value.password)
        .then(
          (res: any) => {
            resolve(res);
          },
          err => reject(err)
        );
    });
  }

  doFacebookLogin() {
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.FacebookAuthProvider();
      this._afAuth.auth.signInWithPopup(provider).then(
        res => {
          this._router.navigate(["/", "core"]);
          resolve(res);
        },
        err => {
          console.log(err);
          reject(err);
        }
      );
    });
  }

  doGoogleLogin() {
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope("profile");
      provider.addScope("email");

      this._afAuth.auth.signInWithPopup(provider).then(res => {
        this._router.navigate(["/", "core"]);
        resolve(res);
      });
    });
  }

  isAuthenticated(): any {
    return this._afAuth.authState.pipe(
      switchMap(auth => {
        if (auth) {
          return of(true);
        } else {
          return of(false);
        }
      })
    );
  }

  getUser() {
    return this._afAuth.authState.pipe(
      map(auth => {
        return {
          displayName: auth.displayName,
          email: auth.email
        };
      })
    );
  }

  doLogout() {
    firebase
      .auth()
      .signOut()
      .then(() => {
        this._router.navigate(["/auth/login"]);
      });
  }
}
