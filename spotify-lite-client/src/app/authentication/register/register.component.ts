import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { Router } from "@angular/router";
import { AuthenticationService } from "../services/auth/authentication.service";

@Component({
  selector: "spotify-lite-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _authService: AuthenticationService,
    private _router: Router
  ) {}

  ngOnInit() {
    this.initializeRegisterFormForm();
  }

  get fields() {
    return this.registerForm.controls;
  }

  initializeRegisterFormForm() {
    this.registerForm = this._fb.group({
      username: new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ]),
      firstname: new FormControl("", [Validators.required]),
      lastname: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required, Validators.email]),
      password1: new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ]),
      password2: new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ])
    });
  }

  onSubmit() {
    console.warn(this.registerForm.value);

    this._authService.doRegister(this.registerForm.value).then(
      res => {
        console.log(res);
        this._router.navigate(["/", "core"]);
      },
      err => {
        console.log(err);
      }
    );
  }
}
