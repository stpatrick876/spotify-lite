import { Component, OnInit, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "spotify-lite-confirm-delete",
  templateUrl: "./confirm-delete.component.html",
  styleUrls: ["./confirm-delete.component.scss"]
})
export class ConfirmDeleteComponent implements OnInit {
  @Output() closePopup: EventEmitter<boolean> = new EventEmitter();

  constructor() {}

  ngOnInit() {}
}
