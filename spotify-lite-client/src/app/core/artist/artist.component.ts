import { Component, OnInit } from "@angular/core";
import { Apollo } from "apollo-angular";
import { ActivatedRoute, Router } from "@angular/router";
import { getArtistById } from "../queries.gql";
import { Album } from "src/data-models/Album";
import { PlayerService } from "../services/player/player.service";
import { Song } from "src/data-models/Song";
import { OPTION_ITEM } from "../options-menu/options-menu.component";

@Component({
  selector: "spotify-lite-artist",
  templateUrl: "./artist.component.html",
  styleUrls: ["./artist.component.scss"]
})
export class ArtistComponent implements OnInit {
  artist: any;
  optionsAlowed: OPTION_ITEM[] = [OPTION_ITEM.ADD_PLAYLIST];

  constructor(
    private _appollo: Apollo,
    private _route: ActivatedRoute,
    private _router: Router,
    private _playerSrv: PlayerService
  ) {}

  ngOnInit() {
    const artistId = this._route.snapshot.paramMap.get("id");
    if (artistId) {
      this._appollo
        .watchQuery({
          query: getArtistById,
          variables: {
            artistId
          }
        })
        .valueChanges.subscribe((result: any) => {
          console.log(result, "results");
          this.artist = result.data.getArtistById;
        });
    }
  }

  onGoToAlbum(album: Album) {
    if (album && album.album_id) {
      this._router.navigate(["/core/album", album.album_id]);
    }
  }

  onTrackClick(song: Song) {
    this._playerSrv.currentSong = song;
    this._playerSrv.setTrack(song, this.artist.topTracks, true);

    // this._playerSrv.playSong();
  }
}
