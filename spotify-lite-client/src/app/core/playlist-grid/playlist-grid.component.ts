import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "spotify-lite-playlist-grid",
  templateUrl: "./playlist-grid.component.html",
  styleUrls: ["./playlist-grid.component.scss"]
})
export class PlaylistGridComponent implements OnInit {
  @Input() playlists: any[];
  @Output() selectPlaylist: EventEmitter<any> = new EventEmitter();
  constructor() {}

  ngOnInit() {}
}
