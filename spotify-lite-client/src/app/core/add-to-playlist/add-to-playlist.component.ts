import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  ComponentRef
} from "@angular/core";
import { Apollo } from "apollo-angular";
import { getPlaylistByUsername, addToPlaylist } from "../queries.gql";
import { AuthenticationService } from "src/app/authentication/services/auth/authentication.service";
import { Song } from "src/data-models/Song";
import { Overlay, OverlayConfig } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";
import { CreatePlaylistComponent } from "../create-playlist/create-playlist.component";

@Component({
  selector: "spotify-lite-add-to-playlist",
  templateUrl: "./add-to-playlist.component.html",
  styleUrls: ["./add-to-playlist.component.scss"]
})
export class AddToPlaylistComponent implements OnInit {
  @Output() closePopup: EventEmitter<boolean> = new EventEmitter();
  user: any;
  playlists: any[];
  songToAdd: Song;
  constructor(
    private _apollo: Apollo,
    private _authSrv: AuthenticationService,
    private _overlay: Overlay
  ) {}

  ngOnInit() {
    this._authSrv.getUser().subscribe((auth: any) => {
      console.log(auth);
      this.user = auth;
      this.getPlaylists();
    });
  }

  getPlaylists() {
    this._apollo
      .watchQuery({
        query: getPlaylistByUsername,
        variables: {
          userName: this.user.displayName
        },
        fetchPolicy: "no-cache"
      })
      .valueChanges.subscribe((result: any) => {
        console.log(result, "results");
        this.playlists = result.data.getPlaylistByUsername;
        console.log("ppsps ", this.playlists);
      });
  }
  addToPlaylist(playlist: any) {
    console.log(playlist, this.songToAdd);

    this._apollo
      .mutate({
        mutation: addToPlaylist,
        variables: {
          playlistId: playlist.playlist_id,
          trackId: this.songToAdd.track_id
        }
      })
      .subscribe(
        ({ data }) => {
          console.log("got data", data);
          //this._router.navigate(["/core/yourMusic"]);
          this.closePopup.emit(true);
        },
        error => {
          console.log("there was an error sending the query", error);
        }
      );
  }

  lauchCreatePlaylistPopup() {
    const positionStrategy = this._overlay
      .position()
      .global()
      .centerHorizontally()
      .centerVertically();

    const overlayConfig = new OverlayConfig({
      hasBackdrop: true,
      backdropClass: "spotify-lite-playlist-overlay-backdrop",
      panelClass: "spotify-lite-playlist-overlay-pannel",
      scrollStrategy: this._overlay.scrollStrategies.block(),
      positionStrategy
    });
    const overlayRef = this._overlay.create(overlayConfig);
    const createPlaylistPortal = new ComponentPortal(CreatePlaylistComponent);
    const compRef: ComponentRef<CreatePlaylistComponent> = overlayRef.attach(
      createPlaylistPortal
    );
    compRef.instance.closePopup.subscribe(() => {
      overlayRef.detach();
      this.getPlaylists();
    });
  }
}
