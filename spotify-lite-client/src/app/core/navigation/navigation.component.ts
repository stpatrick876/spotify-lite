import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "src/app/authentication/services/auth/authentication.service";

@Component({
  selector: "spotify-lite-navigation",
  templateUrl: "./navigation.component.html",
  styleUrls: ["./navigation.component.scss"]
})
export class NavigationComponent implements OnInit {
  user: any;

  constructor(private _authSrv: AuthenticationService) {}

  ngOnInit() {
    this._authSrv.getUser().subscribe((auth: any) => {
      this.user = auth;
    });
  }
}
