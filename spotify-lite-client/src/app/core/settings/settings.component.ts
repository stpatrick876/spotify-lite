import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "src/app/authentication/services/auth/authentication.service";

@Component({
  selector: "spotify-lite-settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.scss"]
})
export class SettingsComponent implements OnInit {
  user: any;
  constructor(private _authSrv: AuthenticationService) {}

  ngOnInit() {
    this._authSrv.getUser().subscribe((auth: any) => {
      console.log(auth);
      this.user = auth;
    });
  }
}
