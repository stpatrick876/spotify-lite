import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Album } from "src/data-models/Album";
import { PlayerService } from "../services/player/player.service";
import { Song } from "src/data-models/Song";
import { Apollo } from "apollo-angular";
import { getAlbumById } from "../queries.gql";
import { OPTION_ITEM } from "../options-menu/options-menu.component";

@Component({
  selector: "spotify-lite-album",
  templateUrl: "./album.component.html",
  styleUrls: ["./album.component.scss"]
})
export class AlbumComponent implements OnInit {
  album: Album;
  optionsAlowed: OPTION_ITEM[] = [OPTION_ITEM.ADD_PLAYLIST];
  constructor(
    private _playerSrv: PlayerService,
    private _route: ActivatedRoute,
    private _appollo: Apollo
  ) {}

  ngOnInit() {
    const albumId = this._route.snapshot.paramMap.get("id");
    if (albumId) {
      this._appollo
        .watchQuery({
          query: getAlbumById,
          variables: {
            _albumId: albumId
          }
        })
        .valueChanges.subscribe((result: any) => {
          console.log(result, "results");
          this.album = result.data.getAlbumById;
        });
    }
  }

  onTrackClick(song: Song) {
    this._playerSrv.currentSong = song;
    this._playerSrv.setTrack(song, this.album.tracks, true);

    // this._playerSrv.playSong();
  }
}
