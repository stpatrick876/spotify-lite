import gql from "graphql-tag";

export const getAlbumRecomendationsQuery = gql`
  {
    AlbumRecomendations {
      album_id
      album_name
      album_artwork
      artist_name
    }
  }
`;

export const getAlbumById = gql`
  query($_albumId: String!) {
    getAlbumById(albumId: $_albumId) {
      album_id
      album_name
      artist_name
      genre_name
      album_artwork
      artist_id
      tracks {
        track_id
        album_id
        track_name
        track_duration
        artist_name
        track_path
        album_artwork
        track_plays
      }
    }
  }
`;

export const getArtistById = gql`
  query($artistId: String!) {
    getArtistById(artistId: $artistId) {
      artist_name
      artist_id
      topTracks {
        track_id
        track_name
        track_duration
      }
      albums {
        album_id
        album_name
        album_artwork
        artist_name
      }
      eps {
        album_id
        album_name
        album_artwork
        artist_name
      }
    }
  }
`;

export const updateTrackPlays = gql`
  mutation($trackId: String!) {
    updateTrackPlays(trackId: $trackId) {
      track_id
      track_plays
    }
  }
`;

export const createPlaylist = gql`
  mutation($playlistName: String!, $playlistOwner: String!) {
    createPlaylist(playlistName: $playlistName, playlistOwner: $playlistOwner) {
      playlist_id
    }
  }
`;

export const getPlaylistByUsername = gql`
  query($userName: String!) {
    getPlaylistByUsername(userName: $userName) {
      playlist_id
      playlist_name
    }
  }
`;

export const getPlaylistById = gql`
  query($playlistId: Int!) {
    getPlaylistById(playlistId: $playlistId) {
      playlist_id
      playlist_name
      tracks {
        track_id
        album_id
        track_name
        track_duration
        artist_name
        track_path
        album_artwork
      }
    }
  }
`;

export const delPlaylistById = gql`
  mutation($playlistId: Int!) {
    delPlaylistById(playlistId: $playlistId)
  }
`;

export const addToPlaylist = gql`
  mutation($playlistId: Int!, $trackId: String!) {
    addToPlaylist(playlistId: $playlistId, trackId: $trackId)
  }
`;

export const deleteSongFromPlaylist = gql`
  mutation($playlistId: Int!, $trackId: String!) {
    deleteSongFromPlaylist(playlistId: $playlistId, trackId: $trackId)
  }
`;
