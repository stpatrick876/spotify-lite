import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Album } from "src/data-models/Album";

@Component({
  selector: "spotify-lite-album-grid",
  templateUrl: "./album-grid.component.html",
  styleUrls: ["./album-grid.component.scss"]
})
export class AlbumGridComponent implements OnInit {
  @Input() albums: Album[];
  @Output() goToAlbum: EventEmitter<Album> = new EventEmitter();
  constructor() {}

  ngOnInit() {}
}
