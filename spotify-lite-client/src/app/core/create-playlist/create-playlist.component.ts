import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
import { AuthenticationService } from "src/app/authentication/services/auth/authentication.service";
import { Apollo } from "apollo-angular";
import { createPlaylist } from "../queries.gql";

@Component({
  selector: "spotify-lite-create-playlist",
  templateUrl: "./create-playlist.component.html",
  styleUrls: ["./create-playlist.component.scss"]
})
export class CreatePlaylistComponent implements OnInit {
  user: any;
  playlistForm: FormGroup;
  @Output() closePopup: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private _fb: FormBuilder,
    private _authSrv: AuthenticationService,
    private _apollo: Apollo
  ) {}

  ngOnInit() {
    this._authSrv.getUser().subscribe((auth: any) => {
      console.log(auth);
      this.user = auth;
    });
    this.playlistForm = this._fb.group({
      name: new FormControl("", [Validators.required])
    });
  }

  onSubmit() {
    console.warn(this.playlistForm.value);

    this._apollo
      .mutate({
        mutation: createPlaylist,
        variables: {
          playlistName: this.playlistForm.value.name,
          playlistOwner: this.user.displayName
        }
      })
      .subscribe(
        ({ data }) => {
          console.log("got data", data);
          this.closePopup.emit(true);
        },
        error => {
          console.log("there was an error sending the query", error);
        }
      );
  }
}
