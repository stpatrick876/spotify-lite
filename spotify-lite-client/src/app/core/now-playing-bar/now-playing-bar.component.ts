import { Component, OnInit, HostListener } from "@angular/core";
import { PlayerService } from "../services/player/player.service";
import { environment } from "src/environments/environment";
import { interval } from "rxjs";
import { tap } from "rxjs/operators";
import { Utils } from "../utils";
import { Apollo } from "apollo-angular";
import { getAlbumRecomendationsQuery, getAlbumById } from "../queries.gql";

@Component({
  selector: "spotify-lite-now-playing-bar",
  templateUrl: "./now-playing-bar.component.html",
  styleUrls: ["./now-playing-bar.component.scss"]
})
export class NowPlayingBarComponent implements OnInit {
  songProgress: any;
  songProgressPercentage: any;
  volumePercentage: any;
  utils = Utils;
  private _mousedDown: boolean;
  @HostListener("document:mouseup", ["$event"])
  onMouseUp() {
    this._mousedDown = false;
  }
  constructor(private _playerSrv: PlayerService, private _appollo: Apollo) {}

  ngOnInit() {
    this._appollo
      .watchQuery({
        query: getAlbumRecomendationsQuery
      })
      .valueChanges.subscribe((result: any) => {
        const initialTracksToPlay = result.data.AlbumRecomendations[0];

        if (initialTracksToPlay) {
          this._appollo
            .watchQuery({
              query: getAlbumById,
              variables: {
                _albumId: initialTracksToPlay.album_id
              }
            })
            .valueChanges.subscribe((result: any) => {
              const { tracks } = result.data.getAlbumById;
              this._playerSrv.setTrack(tracks[0], tracks, false);
            });
        }

        this.checkProgress().subscribe();
        this._playerSrv.volume$.subscribe(v => {
          this.volumePercentage = v;
        });

        document
          .querySelector(".now-playing-bar-container")
          .addEventListener("mousedown touchstart mousemove touchmove", e => {
            e.preventDefault();
          });
      });
  }

  checkProgress() {
    return interval(1000).pipe(
      tap(() => {
        const currentTime = this._playerSrv.currentTime() | 0;
        if (
          this._playerSrv.currentTrack &&
          this._playerSrv.currentTrack.track_duration
        ) {
          const track_duration = Utils.fancyTimeFormat(
            this._playerSrv.currentTrack.track_duration
          );
          const m = track_duration.split(":")[0];
          const s = track_duration.split(":")[1];
          const totalSecs = Number(m) * 60 + Number(s);
          this.songProgressPercentage = Math.round(
            (currentTime / totalSecs) * 100
          );
        }
        this.songProgress = Utils.msToTime(currentTime);
      })
    );
  }

  fullS3Path(endpoint: string): string {
    return endpoint ? `${environment.s3Base}${endpoint}` : "";
  }

  onProgressMouseDown() {
    this._mousedDown = true;
  }

  onProgressMouseMove(e: any) {
    if (this._mousedDown) {
      this.timeFromOfset(e);
    }
  }
  onProgressMouseUp(e: any) {
    this.timeFromOfset(e);
  }

  onProgressMouseDownVolume() {
    this._mousedDown = true;
  }

  onProgressMouseMoveVolume(e: any) {
    if (this._mousedDown) {
      this.setVolume(e);
    }
  }
  onProgressMouseUpVolume(e: any) {
    this.setVolume(e);
  }
  setVolume(e) {
    const percentage =
      e.offsetX / document.getElementById("volumebar").clientWidth;
    if (percentage >= 0 && percentage <= 1) {
      this._playerSrv.volume = percentage;
    }
  }

  timeFromOfset(e) {
    const perc =
      (e.offsetX / document.getElementById("progressbar").clientWidth) * 100;

    const seconds = this._playerSrv.getDuration() * (perc / 100);
    this._playerSrv.setSeek(seconds);
  }
}
