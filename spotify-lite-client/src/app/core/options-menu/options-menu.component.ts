import {
  Component,
  OnInit,
  Input,
  ElementRef,
  Renderer2,
  HostListener,
  Inject,
  EventEmitter,
  Output
} from "@angular/core";
import { DOCUMENT } from "@angular/platform-browser";

export interface Option {
  label: string;
  value: OPTION_ITEM;
}

export enum OPTION_ITEM {
  ADD_PLAYLIST,
  REMOVE_PLAYLIST
}
@Component({
  selector: "spotify-lite-options-menu",
  templateUrl: "./options-menu.component.html",
  styleUrls: ["./options-menu.component.scss"]
})
export class OptionsMenuComponent implements OnInit {
  @Input() options: Option[];
  showing: boolean;
  @HostListener("window:scroll", [])
  onWindowScroll() {
    this.hideMenu();
  }
  @Output() selected: EventEmitter<Option> = new EventEmitter();
  constructor(
    private _el: ElementRef,
    private _renderer: Renderer2,
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngOnInit() {
    this._renderer.listen(this.document, "click", e => {
      if (this.showing) {
        if (
          !e.target.classList.contains("track__options") &&
          !e.target.classList.contains("track__options__btn") &&
          !e.target.classList.contains("options-menu__item")
        ) {
          this.hideMenu();
        }
      }
    });
    this._renderer.listen(
      this.document.querySelector(".content"),
      "scroll",
      () => {
        this.hideMenu();
      }
    );
  }
  showMenu(e: any) {
    const menu = this._el.nativeElement.querySelector(".options-menu");
    const menuWidth = menu.clientWidth;
    const offsetLeft = e.srcElement.offsetLeft - menuWidth;
    const offsetTop = e.srcElement.offsetTop;
    this._renderer.setStyle(menu, "top", `${offsetTop}px`);
    this._renderer.setStyle(menu, "left", `${offsetLeft}px`);
    this._renderer.setStyle(menu, "display", "inline");
    this.showing = true;
  }
  hideMenu() {
    const menu = this._el.nativeElement.querySelector(".options-menu");
    if (this.showing) {
      this._renderer.setStyle(menu, "display", "none");
      this.showing = true;
    }
  }

  onSelction(option: Option) {
    console.log("selected option", option);
    this.hideMenu();
    this.selected.emit(option);
  }
}
