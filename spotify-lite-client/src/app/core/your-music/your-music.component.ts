import { Component, OnInit, ComponentRef } from "@angular/core";
import { ComponentPortal } from "@angular/cdk/portal";
import { Overlay, OverlayConfig } from "@angular/cdk/overlay";
import { CreatePlaylistComponent } from "../create-playlist/create-playlist.component";
import { AuthenticationService } from "src/app/authentication/services/auth/authentication.service";
import { Apollo } from "apollo-angular";
import { getPlaylistByUsername } from "../queries.gql";
import { Router } from "@angular/router";

@Component({
  selector: "spotify-lite-your-music",
  templateUrl: "./your-music.component.html",
  styleUrls: ["./your-music.component.scss"]
})
export class YourMusicComponent implements OnInit {
  user: any;
  playlists: any[];

  constructor(
    private _overlay: Overlay,
    private _authSrv: AuthenticationService,
    private _apollo: Apollo,
    private _router: Router
  ) {}

  ngOnInit() {
    this._authSrv.getUser().subscribe((auth: any) => {
      console.log(auth);
      this.user = auth;
      this.getPlaylists();
    });
  }

  getPlaylists() {
    this._apollo
      .watchQuery({
        query: getPlaylistByUsername,
        variables: {
          userName: this.user.displayName
        },
        fetchPolicy: "no-cache"
      })
      .valueChanges.subscribe((result: any) => {
        console.log(result, "results");
        this.playlists = result.data.getPlaylistByUsername;
      });
  }
  lauchCreatePlaylistPopu() {
    const positionStrategy = this._overlay
      .position()
      .global()
      .centerHorizontally()
      .centerVertically();

    const overlayConfig = new OverlayConfig({
      hasBackdrop: true,
      backdropClass: "spotify-lite-playlist-overlay-backdrop",
      panelClass: "spotify-lite-playlist-overlay-pannel",
      scrollStrategy: this._overlay.scrollStrategies.block(),
      positionStrategy
    });
    const overlayRef = this._overlay.create(overlayConfig);
    const createPlaylistPortal = new ComponentPortal(CreatePlaylistComponent);
    const compRef: ComponentRef<CreatePlaylistComponent> = overlayRef.attach(
      createPlaylistPortal
    );
    compRef.instance.closePopup.subscribe(() => {
      this.getPlaylists();

      overlayRef.detach();
    });
  }

  goToPlaylist(playlist: any) {
    if (playlist) {
      this._router.navigate(["/core/playlist", playlist.playlist_id]);
    }
  }
}
