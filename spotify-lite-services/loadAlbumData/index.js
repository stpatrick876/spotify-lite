"use strict";
var AWS = require("aws-sdk"),
  documentClient = new AWS.DynamoDB.DocumentClient({ region: "us-east-1" });
const jsonfile = require("jsonfile");
const Chance = require("chance");

const chance = new Chance();
const nextAlbumId = () => `al_${chance.hash({ length: 15 })}`;
const nextSongId = () => `so_${chance.hash({ length: 15 })}`;
const nextSongTitle = () =>
  chance.word({ length: chance.integer({ min: 5, max: 10 }) });
const getData = async () => {
  const file =
    "/Users/kemarbell/Desktop/New Workspace/Spotify-Lite/spotify-lite-services/loadAlbumData/data.json";

  return await new Promise((resolve, reject) => {
    jsonfile
      .readFile(file)
      .then(albums => {
        albums.forEach(album => {
          const albumId = nextAlbumId();
          album.id = albumId;
          album.updatedAt = new Date().getTime();
          album.songs = [...album.songs, ...album.songs, ...album.songs];
          album.songs.forEach(song => {
            song.albumId = albumId;
            song.id = nextSongId();
            song.artist = album.artist;
            song.title = nextSongTitle();
          });
        });

        resolve(albums);
      })
      .catch(error => reject(error));
  });
};

module.exports.loadAlbums = async (event, context, callback) => {
  return await new Promise((resolve, reject) => {
    const itemsArray = [];

    getData().then(albums => {
      albums.forEach(album => {
        const item = {
          PutRequest: {
            Item: album
          }
        };

        if (item) {
          itemsArray.push(item);
        }
      });

      const params = {
        RequestItems: {
          Albums: itemsArray
        }
      };

      documentClient.batchWrite(params, function(err, data) {
        if (err) {
          console.log(err);
        } else {
          console.log("Added " + itemsArray.length + " items to DynamoDB");
        }
      });
    });
  });
};
