"use strict";
var AWS = require("aws-sdk"),
  documentClient = new AWS.DynamoDB.DocumentClient();

module.exports.v = async (event, context, callback) => {
  const params = {
    TableName: "Albums",
    Key: {
      id: event.pathParameters.albumId
    }
  };
  console.log("param", params);

  return await new Promise((resolve, reject) => {
    documentClient.get(params, function(error, data) {
      if (error) {
        console.log("geyt err", error);
        reject({
          statusCode: 400,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
          },
          error: `Could not read movies: ${error.stack}`
        });
      } else {
        resolve({
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
          },
          body: JSON.stringify(data)
        });
      }
    });
  });
};
